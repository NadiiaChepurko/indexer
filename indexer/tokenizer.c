#include "tokenizer.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>
#include <unistd.h>
#include <dirent.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


typedef struct listNode{
    char *filename;
    int count;
    struct listNode *next;
}listNodePtr;

typedef struct node{
    struct node *children; //front pointer
    int isWord; //indicated if current character is the end of the dictionary word, 1 - indicates word, 0 - not a word.
    int *usedIndices; //front pointer to array of int indices
    int isParent;
    struct listNode *list;
}NodePtr;

NodePtr root;
char *initPath;
char *relativePath;
int capacity;

void destroyTrie(){
    free(relativePath);
}

int charToIndex(char ch){
    if(ch >= 'a' && ch <= 'z'){
        return ch - 'a' + 10;
    }else if(ch >= 'A' && ch <= 'Z'){
        return ch - 'A' + 10;
    }else if(ch >= '0' && ch <= '9'){
        return ch - '0';
    }else{
        return 37; //not an alphanum char
    }
}

void getRelativePath(char *s)
{
    int rl = (int)strlen(initPath);
    int l = (int)strlen(s);
    relativePath = calloc((l - rl + 1), 1);
    char *bufferTemp = relativePath;
    char *s2 = &s[rl + 1];
    
    while(*s2){
        *bufferTemp = *s2;
        s2++;
        bufferTemp++;
    }
}

int updateFileName(struct listNode *list, char *fileName){
    listNodePtr *temp = list;
    if(list -> count == 0){ //list is empty
        list -> filename = calloc(strlen(fileName)+1, sizeof(char));
        strcpy(list -> filename, fileName);
        list -> count++;
        return 1;
    }else{
        while(temp -> next != NULL && strcmp(temp -> filename, fileName) != 0){
                temp = temp -> next;
        }
        
        if(strcmp(temp -> filename, fileName) == 0){
            temp -> count++;
            return 0;
        }else if(temp -> next == NULL) {
            temp -> next = calloc(1, sizeof(struct listNode));
            temp = temp -> next;
            temp -> filename = calloc(strlen(fileName)+1, sizeof(char));
            strcpy(temp -> filename, fileName);
            temp -> count++;
            return 1;
        }
    }
    return 0;
}

int doesTrieExist = 0;

void storeFileTokens(FILE *file, char *fileName){
    
    if (doesTrieExist == 0){
        root.isWord = 0;
        root.children = calloc(36, sizeof(struct node));
        root.usedIndices = calloc(36, sizeof(int));
        root.list = calloc(1, sizeof(struct listNode));
        root.isParent = 0;
        doesTrieExist = 1;
        capacity = 0;
    }
    
    NodePtr* temp = &root;
    
    int letterBefore = 0;
    int c = 0;
    int cFlag = 0;
    int count = 0;
    while((c = getc(file)) != EOF){
        
        if((c = charToIndex(c)) <= 35 && c >= 0){
            if (temp -> children[c].children == NULL){
                temp -> children[c].children = calloc(36, sizeof(struct node));
                temp -> children[c].isWord = 0;
                temp -> children[c].usedIndices = calloc(36, sizeof(int));
                temp -> children[c].list = calloc(1, sizeof(struct listNode));
                temp -> usedIndices[c] = 1;
                temp -> isParent = 1;
                temp = &temp -> children[c];
                letterBefore = 1;
                cFlag = 1;
                count++;
            }else{
                temp = &temp -> children[c];
                letterBefore = 1;
                cFlag = 1;
                count++;
            }
        }else if (letterBefore){
            temp -> isWord = 1; //last character indicates that it was the end of the word
            updateFileName(temp -> list, fileName); //if new fileName was added, return +1
            temp = &root;
            letterBefore = 0;
            cFlag = 0;
            if(count > capacity) capacity = count;
            count = 0;
        }
        
    }
    
    if(cFlag == 1){
        temp -> isWord = 1; //last character indicates that it was the end of the word
        updateFileName(temp -> list, fileName); //if new fileName was added, return +1
    }
    if(count > capacity) capacity = count;
}

/* ------------------ sort filenames ------------------- */


listNodePtr* getFinalList(listNodePtr *list){
    
    listNodePtr *newFront = NULL;
    listNodePtr *newFrontTemp = newFront;
    
    listNodePtr *tempList;
    listNodePtr *lergest;
    
    listNodePtr *prev = NULL;
    listNodePtr *largPrev = NULL;
    
    while(list != NULL){
        tempList = list -> next;
        lergest = list;
        prev = list;
        largPrev = NULL;
        while(tempList != NULL){
            if(tempList -> count > lergest -> count){
                largPrev = prev;
                lergest = tempList;
            }
            prev  = tempList;
            tempList = tempList -> next;
        }
        
        if(largPrev == NULL){//front of list is smallest
            if(newFront == NULL){
                newFront = list;
                newFrontTemp = newFront;
                list = list -> next;
            }else{
                newFrontTemp -> next = list;
                newFrontTemp = newFrontTemp -> next;
                list = list -> next;
            }
        }else{
            if(newFront == NULL){
                newFront = lergest;
                newFrontTemp = newFront;
                largPrev -> next = lergest -> next;
                newFrontTemp -> next = NULL;
            }else{
                newFrontTemp -> next = lergest;
                largPrev -> next = lergest -> next;
                newFrontTemp = newFrontTemp -> next;
                newFrontTemp -> next = NULL;
            }
            
            
        }
        
    }
    
    return newFront;
    
}
/* ------------------- print part ---------------------- */

void printList(listNodePtr *list, FILE *file){
    while(list != NULL && list -> count != 0){
        getRelativePath(list -> filename);
        fprintf(file, "%s %d ", relativePath, list -> count); //<<<<<------- ???? :D
        list = list -> next;
    }
    printf("\n");
}


void recPrint(FILE *file, char *buffer, char *freeCh, NodePtr* temp){
    
    if(temp -> isParent == 0){ //no superwords in this branch anymore
        temp -> list = getFinalList(temp -> list);
        fprintf(file, "<list> %s\n", buffer);
        printList(temp -> list, file);
        fprintf(file, "\n<list>\n");
        
        free(temp->children);
        free(temp->list);
        free(temp->usedIndices);
        return;
    }
    
    if(temp -> isWord){
        temp -> list = getFinalList(temp -> list);
        fprintf(file, "<list> %s\n", buffer);
        printList(temp -> list, file);
        fprintf(file, "<list>\n");

    }
    

    int *intArr = temp -> usedIndices;
    int i = 0;
    for(i = 0; i < 36; i++){
        if(intArr[i] == 1){
            if(i > 9){
                *freeCh = i - 10 + 'a' ;
            }else{
                *freeCh = i + '0';
            }
            freeCh++;
            recPrint(file, buffer,freeCh, &temp -> children[i]);
            
            //now we need to remove all extra characters till the next turn
            freeCh--;
            *freeCh = '\0';
            
            free(temp->children);
            free(temp->list);
            free(temp->usedIndices);
        }
    }
}

void printTrie(FILE *file, char *initialPath){
    
    initPath = initialPath;
    if(root.isParent == 0) return;
    char *buffer;
    char *freeCh;
    int *intArr = root.usedIndices;
    
    int i = 0;

    for(i = 0; i < 36; i++){
        
        if(intArr[i] == 1){
            buffer = calloc(capacity + 1, sizeof(char));
            freeCh = buffer;
            if(i > 9){
                *freeCh = i - 10 + 'a' ;
            }else{
                *freeCh = i + '0';
            }
            
            freeCh++;
            recPrint(file, buffer, freeCh, &root.children[i]);
            free(buffer);
        }
    }
    
    free(root.children);
    free(root.list);
    free(root.usedIndices);
}

