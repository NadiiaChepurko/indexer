#include "tokenizer.h"
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>
#include <unistd.h>
#include <dirent.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int traverse(char *curpath, char *path);
void readFile();

int isHidden(char *str){
    if(*str == '.' ) return 1;
    return 0;
}

char* concat(char *s1, char *s2)
{
    char *result = calloc(strlen(s1)+strlen(s2)+1, sizeof(char));
    if(result == NULL)printf("calloc error occured\n");
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

int main(int argc, const char * argv[]) {
    
    if (argc != 3){
        printf("Invalid input. Please enter indexer file name and target directory for indexing.\n");
        return 1;
    }
    
    struct stat s;
    char *filePath = calloc(strlen(argv[1]) + 1, sizeof(char));;
    char *filePathTop = strrchr(argv[1], '/');
    filePath = memcpy(filePath, argv[1], strlen(argv[1]) - strlen(filePathTop) +1);
    
    /* ----- check if file exists  ---- */
    int err = stat(filePath, &s);
    if(-1 == err) {
        if(ENOENT == errno) {
            printf ("Error getting stats, reason is : %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }else{
            printf ("Error getting stats, reason is : %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
    }
    
    if( access( argv[1], F_OK ) != -1 ) {
        // file exists
        int decision = -1;
        int status;
        printf("File with this name already exists. Do you want to override it? (Enter 1 if yes and 0 otherwise):");
        
        while(decision != 0 && decision != 1){
            status = scanf("%d",&decision);
            if(status != 1){
                fprintf(stderr, "Bad decision. Don't do it next time.\n");
                exit(EXIT_FAILURE);
            }
            if(decision != 0 && decision != 1)
                printf("Enter 1 if yes and 0 otherwise: ");
        }
        if(decision == 0) return 0;
    }
    
    char *initialPathName = calloc(strlen(argv[2]) + 1, sizeof(char));
    initialPathName = strcpy(initialPathName, argv[2]);
    
    
    /* ----- check if directory exists ---- */
    err = stat(initialPathName, &s);
    if(-1 == err) {
        if(ENOENT == errno) {
            fprintf(stderr, "Directory does not exist\n");
            exit(EXIT_FAILURE);
        }else{
            perror("stat");
            exit(EXIT_FAILURE);
        }
    }

    traverse(NULL, initialPathName);
    
    FILE *indexer = fopen(argv[1], "w");
    printTrie(indexer, initialPathName);
    
    if(fclose(indexer) != 0)
        printf ("Error closing indexer file, reason is : %s\n", strerror(errno));

    free(initialPathName);
    free(filePath);
    destroyTrie();
    return 0;
}

int traverse(char *curpath, char *path)
{
    char ep[PATH_MAX];
    char p[PATH_MAX];
    DIR *dirp;
    struct dirent entry;
    struct dirent *result;
    struct stat st;
    
    if (curpath != NULL){
        snprintf(ep, sizeof(ep), "%s/%s", curpath, path);
    }else{
        snprintf(ep, sizeof(ep), "%s", path);
    }
    if (stat(ep, &st) == -1){
        printf ("Error getting stats, reason is : %s\n", strerror(errno));
        return -1;
    }
    if ((dirp = opendir(ep)) == NULL){
        printf ("Error opening file, reason is : %s\n", strerror(errno));
        return -1;
    }
    
    for (;;) {
        result = NULL;
        
        if (readdir_r(dirp, &entry, &result) == -1) {
            printf ("Error reading directory, reason is : %s\n", strerror(errno));
            closedir(dirp);
            return -1;
        }
        
        if (result == NULL) break;
        assert(result == &entry);
        
        if (strcmp(entry.d_name, ".") == 0 || strcmp(entry.d_name, "..") == 0) continue;
        
        if (curpath != NULL){
            snprintf(ep, sizeof(ep), "%s/%s/%s", curpath, path, entry.d_name);
        }else{
            snprintf(ep, sizeof(ep), "%s/%s", path, entry.d_name);
        }
        
        if (stat(ep, &st) == -1) {
            printf ("Error getting stats, reason is : %s\n", strerror(errno));
            closedir(dirp);
            return -1;
        }
        
        if (S_ISREG(st.st_mode) || S_ISDIR(st.st_mode)) { // <<<---------------
            if(S_ISREG(st.st_mode)){
                if(!isHidden(entry.d_name)){
                    readFile(ep);
                    //printf("File: %s \n", entry.d_name);
                }
            }else{
                //printf("Directory: %s \n", entry.d_name);
            }
            //printf("%c %s\n", S_ISDIR(st.st_mode) ? 'd' : 'f', ep);
        }
        
        if (S_ISDIR(st.st_mode) == 0) continue;
        
        if (curpath != NULL){
            snprintf(p, sizeof(p), "%s/%s", curpath, path);
        }
        else{
            snprintf(p, sizeof(p), "%s", path);
        }
        snprintf(ep, sizeof(ep), "%s", entry.d_name);
        traverse(p, ep);
    }
    closedir(dirp);
    return 0;
}


void readFile(char *absoluteFilePath){
    FILE *file = fopen(absoluteFilePath, "r");
    if(file == NULL) {
        fprintf(stderr, "Cannot open the file at: %s\n", absoluteFilePath);
        return;
    }
    storeFileTokens(file, absoluteFilePath);
    
    if(fclose(file) != 0)
        printf ("Error closing file, reason is : %s\n", strerror(errno));
}