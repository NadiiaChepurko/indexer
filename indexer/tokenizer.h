#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

struct node;
struct listNode;

void storeFileTokens(FILE *file, char *fileName);
void printTrie(FILE *file, char *);
void destroyTrie();

